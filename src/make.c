/**
 *  @file make.c
 * 
 *  Wrapper for shelling out to make.
 */

/* Standard Library Headers */
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int make_cmd(char * target)
{
    char command[255] = {0};

    strncpy(command, "make -s ", 9);

    if (NULL != target)
    {
        strncpy(command + 8, target, strlen(target));
    }

    printf("Executing: %s\n", command);

    return system(command);
}