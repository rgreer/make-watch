# make-watch

Continuous runner that watches C code directories and runs make. This can be used to run builds, tests, or any other make targets on changes. The tool is written in C and has a relatively small footprint.

make-watch recursively looks for changes to any `.c` or `.h` files in the current working directory. Specific subdirectories can be provided. 

## Installation

To build and install make-watch, execute the following:
```
make
sudo make install
```

## Usage

After installing make-watch, run it from your project directory using:
```
make-watch -h
make-watch $test_target
```

Additionally, make-watch can be invoked using the `mw` shorthand which is a symlink in the installation directory.

### Help

Print the help options:
```
$ make-watch -h
Usage: make-watch [ OPTIONS ... ] [ TARGET ]

Continuous Make Runner for C Projects

Options:
    -h, --help           Show the help menu.
    -d DIR, --dir DIR    Specify a directory to watch. May be used
                         multiple times. Specifying at least one
                         directory means that only specified
                         directories will be watched.

Positional Arguments:
    TARGET    Make target to run on changes. Default is none.

README available at: <https://gitlab.com/ryan.greer/make-watch>
```

### Examples

To specify the only directories to watch:
```
$ make-watch --dir src --dir include
$ make-watch -d src -d include
```

To specify a make target:
```
$ make-watch my_target
```

## Notes

* Hidden directories (those that start with `.`) are ignored.


