/**
 *  @file make.h
 * 
 *  Function prototypes for make wrapper.
 */

#ifndef __MAKE_H
#define __MAKE_H

/**
 *  Executes `make -s` with the specified target. Fails if target is NULL.
 * 
 *  @param target String representing the make target to execute.
 * 
 *  @return Integer representing the result of calling:
 *              `system("make -s target")`
 */
int make_cmd(char * target);

#endif
