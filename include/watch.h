/**
 *  @file watch.h
 *
 *  This file contains prototypes used by watch.c and available to the rest of
 *  make-watch.
 */

#ifndef __WATCH_H
#define __WATCH_H

/**
 *  Initializes the watch environment.
 * 
 *  @return Integer 0 indicating successful initialization. Return -1 if an
 *          error occurs.
 */
int watch_init();

/**
 *  Adds a directory and all of its subdirectories to the list to
 *  be watched.
 * 
 *  @param directory String representing the directory to watch.
 * 
 *  @return Integer indicating the number of directories added. If directory
 *          has not subdirectories, this function returns 1 on success. If
 *          directory has subdirectories, this function returns a number
 *          greater than 0. If an error occurs, this directory returns -1.
 */
int watch_directory(const char * directory);

/**
 *  Starts the blocking watcher. If this function returns, it is due to an
 *  error.
 * 
 *  @param target String representing the make target to call when a valid
 *                change was observed.
 */
void watch_start(const char * target);

#endif
