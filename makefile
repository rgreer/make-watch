CC=gcc
CFLAGS=-g -Wall -Werror -Wextra

INC=include
BIN=bin
SRC=src

.PHONY: all test doc

all: build

build: clean
	$(CC) -o $(BIN)/make-watch $(SRC)/*.c -I$(INC) $(CFLAGS)

clean:
	rm -rf $(BIN)
	mkdir -p $(BIN)

install:
	cp $(BIN)/make-watch /usr/local/bin
	ln -sf /usr/local/bin/make-watch /usr/local/bin/mw

test: build
	echo testing
